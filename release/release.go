package release

import (
	"fmt"
	"gitlab-runner-group-releaser/client"
	"gitlab-runner-group-releaser/cmd"
	"gitlab-runner-group-releaser/tag"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/samber/lo"
	"github.com/sourcegraph/conc/iter"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/extractor"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/generator"
	gitchangelog "gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/git"
	gitlabchangelog "gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/gitlab"
	"golang.org/x/sync/errgroup"
)

const (
	EnvReleaseGitLabToken = "RELEASER_GITLAB_TOKEN"
	EnvConfigProjectPath  = "RELEASER_CONFIG_PROJECT_PATH"

	TriggerGitLabReleaseJobName = "stable gitlab release"
)

type Release struct {
	Project *Project

	releasesConfig ReleasesConfig
	config         *Config

	dryRun bool
}

type ReleaseEntry struct {
	Project *Project
}

func New(name string, versions ...string) (*Release, error) {
	cfg, err := LoadConfig(os.Getenv(EnvConfigProjectPath))
	if err != nil {
		return nil, err
	}

	releasesConfig := lo.Filter(cfg.Releases, func(r ReleaseConfig, _ int) bool {
		if r.Name == name && len(versions) == 0 {
			return true
		}

		return r.Name == name && lo.Contains(versions, r.Version.String())
	})
	if len(releasesConfig) == 0 {
		return nil, fmt.Errorf("release %q not found in config with versions %q", name, versions)
	}

	proj, found := lo.Find(cfg.Projects, func(p Project) bool {
		return p.Name == name
	})
	if !found {
		return nil, fmt.Errorf("proj %q not found in config", name)
	}

	release := &Release{
		Project:        &proj,
		releasesConfig: releasesConfig,

		config: cfg,
	}

	sort.Sort(release.releasesConfig)

	return release, nil
}

func (r *Release) DryRun() {
	r.dryRun = true
}

func (r *Release) SkipPrepare() {
	r.Project.skipPrepare = true
}

func (r *Release) runProjectConfig(stage Stage, releaseConfig ReleaseConfig) (*ProjectConfig, error) {
	cfg, err := ParseProjectConfig(Context{
		Stage: ContextStage{
			Name: stage,
		},
		Release: ContextRelease{
			VersionObject: releaseConfig.Version,
			Version:       releaseConfig.Version.StringNoPrefix(),

			AppVersionObject: releaseConfig.AppVersion,
			AppVersion:       releaseConfig.AppVersion.StringNoPrefix(),
		},
	}, ProjectConfigPathFromStage(r.Project.Path(), stage))
	if err != nil {
		return nil, err
	}

	if err := cfg.Execute(r); err != nil {
		return nil, err
	}

	return cfg, nil
}

func (r *Release) log() *zerolog.Logger {
	logger := log.With().Str("project", r.Project.Name).Logger()
	return &logger
}

func (r *Release) logWithTag(tag tag.Tag) *zerolog.Logger {
	logger := r.log().With().Stringer("tag", tag).Logger()
	return &logger
}

func (r *Release) Tag() error {
	if err := r.Project.Prepare(r.releasesConfig.Versions()...); err != nil {
		return err
	}

	for _, cfg := range r.releasesConfig {
		if r.gitTagExistsRemotely(cfg.Version, cfg.Variant) {
			r.logWithTag(cfg.Version).Warn().Msg("Tag already exists in remote, skipping...")
			continue
		}

		if err := r.createTag(cfg); err != nil {
			return err
		}
	}

	return nil
	// # For each tag
	// * Important * Tag must be sorted and created in order from oldest to newest so they appear in the correct order in the /tags page of the Project
	// All the checks can be done sequentially or out of order but the final tag creation must be done in order

	//1. git checkout main && git pull && git checkout -b 15-7-stable
	//2. make generate_changelog CHANGELOG_RELEASE=v15.7.0
	//3. Go through each changelog MR entry and verify that:
	//	3.1 Milestone is correctly set to current milestone. If this is a patch release, e.g. 15.6.1 the milestone still needs to be the latest patch - %15.7
	//	3.2 Correct labels are set: maintentance / feature / docs etc. Just check for their existance no need for further and deeper checks
	// 	3.3 Check the title doesn't contain prefixes - feat:, docs:
	// 	3.4 Check the title is properly capitalized so the final changelog is uniform
	//	3.5 Try to collect as much info as possible, avoid failing fast
	//4. git add CHANGELOG.md && git commit -m "Update CHANGELOG for v15.7.0"
	//5. git tag v15.7.0 -m "Version v15.7.0" && git push origin v15.7.0 15-7-stable

	// Notes:
	// All the steps above should not break the workflow if they are ran twice. Changelog should check if it's generated, tag should be checked if it's created etc.

	// TODO: maybe generate release checklist as part of the pipeline

	// TODO: do this in a final step

	// Finally:
	// Each tag should be merged into main
	// ** ONLY ** if we are doing a normal .0 release - the main branch needs to have its version bumped to 15.8.0
	// The command bellow needs to be split into segments

	// git checkout main && \
	// git pull && \
	// git merge --no-ff 15-7-stable && \
	// echo -n "15.8.0" > VERSION && \
	// git add VERSION && \
	// git commit -m "Bump version to 15.8.0" && \
	// git push
}

func (r *Release) Changelog() error {
	if err := r.Project.Prepare(r.releasesConfig.Versions()...); err != nil {
		return err
	}

	// we have already guaranteed that there is at least one tag during initialization
	for _, cfg := range r.releasesConfig {
		t := cfg.Version

		stableBranch := t.AsStableBranch()

		if r.gitBranchExistsRemotely(stableBranch, cfg.Variant) {
			// we are already on the default branch, checkout to stable only if needed
			if err := r.checkoutStableBranch(t, cfg.Variant); err != nil {
				return err
			}
		}

		projectConfig, err := r.runProjectConfig(StageChangelog, cfg)
		if err != nil {
			return err
		}

		if err := r.generateChangelog(cfg, projectConfig); err != nil {
			return err
		}

		_ = r.diffChangelogCommand().Run()
	}

	return nil
}

func (r *Release) checkoutStableBranch(t tag.Tag, v Variant) error {
	return r.ProjectGit().Args("checkout", "--track",
		fmt.Sprintf("%s/%s", r.Project.Remote(v), t.AsStableBranch()),
	).Run()
}

func (r *Release) checkoutNewStableBranch(t tag.Tag) error {
	return r.ProjectGit().Args("checkout", "-b", t.AsStableBranch()).Run()
}

func (r *Release) mustCheckoutStableBranch(t tag.Tag, v Variant) error {
	if r.gitBranchExistsRemotely(t.AsStableBranch(), v) {
		return r.checkoutStableBranch(t, v)
	}

	return r.checkoutNewStableBranch(t)
}

func (r *Release) checkoutMainBranch() error {
	return r.ProjectGit().Args("checkout", r.Project.MainBranch()).Run()
}

func (r *Release) StableBranch() error {
	for _, cfg := range r.releasesConfig {
		if err := r.Project.Prepare(cfg.Version); err != nil {
			return err
		}

		if err := r.createStableBranch(cfg); err != nil {
			return err
		}
	}

	return nil
}

func (r *Release) generateChangelog(cfg ReleaseConfig, c *ProjectConfig) error {
	t := cfg.Version
	mrs, err := r.processReleaseMergeRequests(cfg)
	if err != nil {
		return err
	}

	if len(mrs)+len(c.AdditionalChangelogEntries) == 0 {
		r.logWithTag(t).Info().Msg("No additional changelog entries found either, skipping changelog generation")
		return nil
	}

	previousTag, err := r.Project.PreviousTag(cfg.Version)
	if err != nil {
		return err
	}

	generatorMrs := lo.Map(mrs, func(mr *gitlab.MergeRequest, _ int) gitlabchangelog.MergeRequest {
		var authorName string
		var authorHandle string
		if mr.Author != nil {
			authorName = mr.Author.Name
			authorHandle = mr.Author.Username
		}

		return gitlabchangelog.MergeRequest{
			IID:          mr.IID,
			Title:        mr.Title,
			Labels:       mr.Labels,
			AuthorName:   authorName,
			AuthorHandle: authorHandle,
			URL:          mr.WebURL,
		}
	})

	opts := generator.Opts{
		ProjectID:         r.Project.ID(cfg.Variant),
		Release:           t.String(),
		StartingPoint:     previousTag.String(),
		WorkingDirectory:  r.Project.Path(),
		ChangelogFile:     filepath.Join(r.Project.Path(), "CHANGELOG.md"),
		ConfigFile:        filepath.Join(r.Project.Path(), ".gitlab", "changelog.yml"),
		MRs:               generatorMrs,
		AdditionalEntries: c.AdditionalChangelogEntries,
		OverwriteRelease:  true,
	}

	g, err := generator.New(opts)
	if err != nil {
		return fmt.Errorf("creating changelog generator %w", err)
	}

	if err := g.Generate(); err != nil {
		return fmt.Errorf("generating changelog %w", err)
	}

	return nil
}

func (r *Release) ProjectGit() *cmd.Cmd {
	return cmd.New("git").Wd(r.Project.Path())
}

func (r *Release) gitBranchExistsRemotely(branch string, v Variant) bool {
	return r.ProjectGit().Args("ls-remote", "--exit-code", "--heads", r.Project.Remote(v), branch).Run() == nil
}

func (r *Release) gitTagExistsRemotely(t tag.Tag, v Variant) bool {
	return r.ProjectGit().Args("ls-remote", "--exit-code", "--tags", r.Project.Remote(v), t.String()).Run() == nil
}

func (r *Release) gitBranchExistsLocally(branch string) bool {
	return r.ProjectGit().Args("rev-parse", "--verify", branch).Run() == nil
}

func (r *Release) diffChangelogCommand() cmd.WithArgs {
	return r.ProjectGit().Args("diff", "--exit-code", "CHANGELOG.md")
}

// createStableBranch creates a stable branch for the given tag
// and generates a changelog for it. The changelog will be regenerated accordingly
// if the function is run more than once on an existing branch and changelog
func (r *Release) createStableBranch(releaseConfig ReleaseConfig) error {
	t := releaseConfig.Version

	stableBranch := t.AsStableBranch()

	r.logWithTag(t).
		Info().
		Str("stable_branch", stableBranch).
		Msg("Creating stable branch")

	git := r.ProjectGit()

	if err := r.mustCheckoutStableBranch(t, releaseConfig.Variant); err != nil {
		return err
	}

	cfg, err := r.runProjectConfig(StageStableBranch, releaseConfig)
	if err != nil {
		return err
	}

	if err := r.generateChangelog(releaseConfig, cfg); err != nil {
		return err
	}

	var commands cmd.ManyWithArgs
	if err := r.diffChangelogCommand().Run(); err != nil {
		commands = append(commands, git.Args("add", "CHANGELOG.md"))
		commands = append(commands, git.Args("commit", "-m", fmt.Sprintf("Update CHANGELOG for %s", t.String())))
	}

	if !r.dryRun {
		commands = append(commands, git.Args("push", r.Project.Remote(releaseConfig.Variant), fmt.Sprintf("HEAD:%s", stableBranch)))
	} else {
		log.Warn().Msg("Skipping push to remote because of dry run")
	}

	return commands.Run()
}

func (r *Release) createTag(releaseConfig ReleaseConfig) error {
	t := releaseConfig.Version

	log.Info().Stringer("tag", t).Msg("Creating tag")

	if err := r.checkoutStableBranch(t, releaseConfig.Variant); err != nil {
		return err
	}

	_, err := r.runProjectConfig(StageTag, releaseConfig)
	if err != nil {
		return err
	}

	git := r.ProjectGit()
	commands := cmd.ManyWithArgs{
		git.Args("tag", t.String(), "-m", fmt.Sprintf("Version %s", t.String())),
	}

	if !r.dryRun {
		commands = append(commands, git.Args("push", r.Project.Remote(releaseConfig.Variant), t.String()))
	} else {
		log.Warn().Msg("Skipping push to remote because of dry run")
	}

	return commands.Run()
}

func (r *Release) processReleaseMergeRequests(cfg ReleaseConfig) ([]*gitlab.MergeRequest, error) {
	t := cfg.Version
	r.logWithTag(t).Info().Msg("Extracting release merge requests")

	previousTag, err := r.Project.PreviousTag(cfg.Version)
	if err != nil {
		return nil, err
	}

	mrIIDs, err := extractor.New(gitchangelog.New(r.Project.Path())).
		ExtractMRIIDs(previousTag.String(), "")
	if err != nil {
		return nil, err
	}

	if len(mrIIDs) == 0 {
		r.logWithTag(t).Info().Msg("No merge requests found for this release")
		return nil, nil
	}

	r.logWithTag(t).Info().Ints("mr_iids", mrIIDs).Msg("Extracted release merge requests")

	client, err := r.Project.GitLabClient(cfg.Variant)
	if err != nil {
		return nil, err
	}

	var milestone *gitlab.Milestone
	if !t.IsPatch() {
		r.logWithTag(t).Info().Msg("Listing milestones")
		milestones, _, err := client.Milestones.ListMilestones(r.Project.ID(VariantCanonical), &gitlab.ListMilestonesOptions{
			IncludeParentMilestones: gitlab.Bool(true),
			Search:                  gitlab.String(cfg.Milestone().AsMinor()),
		})
		if err != nil {
			return nil, fmt.Errorf("listing milestones: %w", err)
		}

		milestone, _ = lo.Find(milestones, func(m *gitlab.Milestone) bool {
			return m.Title == cfg.Milestone().AsMinor()
		})
		if milestone == nil {
			return nil, fmt.Errorf("milestone %s not found", t.AsMinor())
		}
	}

	iterator := iter.Mapper[int, *gitlab.MergeRequest]{
		MaxGoroutines: 10,
	}

	r.logWithTag(t).Info().Msg("Processing merge requests")
	mrs, err := iterator.MapErr(mrIIDs, func(mrIID *int) (*gitlab.MergeRequest, error) {
		iid := *mrIID

		r.logWithTag(t).Info().Int("mr_iid", iid).Msg("Processing merge request")

		// If we have a security release we need to try to fetch the MR from the security repo first
		// This is rather naive but works because the difference in MR IIDs between the canonical and security repos
		// is usually big. In the future we should have a way to distinguish between a security MR IID and canonical MR IID.
		variants := []Variant{VariantCanonical}
		if cfg.Variant == VariantSecurity {
			variants = []Variant{
				VariantSecurity,
				VariantCanonical,
			}
		}

		var mr *gitlab.MergeRequest
		for _, v := range variants {
			var err error
			var resp *gitlab.Response
			mr, resp, err = client.MergeRequests.GetMergeRequest(r.Project.ID(v), iid, nil)
			if mr != nil {
				break
			} else if resp != nil && resp.StatusCode == http.StatusNotFound {
				continue
			} else if err != nil {
				return nil, fmt.Errorf("getting merge request %d: %w", iid, err)
			}
		}

		if lo.Contains(mr.Labels, "type::ignore") {
			return nil, nil
		}

		updateOpts := &gitlab.UpdateMergeRequestOptions{}

		updateOpts.MilestoneID, err = r.reconcileMergeRequestMilestoneID(mr, milestone, cfg.Milestone())
		if err != nil {
			return nil, fmt.Errorf("reconciling merge request milestone: %w", err)
		}

		updateOpts.Title = r.reconcileMergeRequestTitle(mr, t)

		if updateOpts.MilestoneID != nil || updateOpts.Title != nil {
			if !r.dryRun {
				for _, v := range variants {
					_, resp, err := client.MergeRequests.UpdateMergeRequest(r.Project.ID(v), iid, updateOpts)
					if resp.StatusCode == http.StatusNotFound {
						continue
					} else if err != nil {
						return nil, fmt.Errorf("updating merge request %d: %w", iid, err)
					}
				}

			} else {
				r.logWithTag(t).Warn().Int("mr_iid", iid).Msg("Skipping merge request update because of dry run")
			}
		}

		if err := r.assertMergeRequestLabels(mr, t); err != nil {
			return nil, fmt.Errorf("asserting merge request labels: %w", err)
		}

		return mr, nil
	})

	if err != nil {
		return nil, err
	}

	return lo.Filter(mrs, func(mr *gitlab.MergeRequest, _ int) bool {
		return mr != nil
	}), nil
}

func (r *Release) reconcileMergeRequestMilestoneID(mr *gitlab.MergeRequest, milestone *gitlab.Milestone, t tag.Tag) (*int, error) {
	if milestone == nil {
		return nil, nil
	}

	if mr.Milestone != nil && mr.Milestone.Title == t.AsMinor() {
		r.logWithTag(t).Info().Int("mr_iid", mr.IID).Msg("Milestone is already correctly set")
		return &mr.Milestone.ID, nil
	}

	r.logWithTag(t).Info().Int("mr_iid", mr.IID).Str("new_milestone", milestone.Title).Msg("Will update milestone")
	return &milestone.ID, nil
}

func (r *Release) reconcileMergeRequestTitle(mr *gitlab.MergeRequest, t tag.Tag) *string {
	// Add more prefixes here
	prefixes := []string{
		"feat:",
		"docs:",
		"doc:",
		"bug:",
		"fix:",
	}

	r.logWithTag(t).Info().Int("md_iid", mr.IID).Str("title", mr.Title).Msg("Processing title")

	title := mr.Title
	titleLower := strings.ToLower(mr.Title)
	for _, prefix := range prefixes {
		if strings.HasPrefix(titleLower, prefix) {
			title = strings.TrimSpace(title[len(prefix):])
			r.logWithTag(t).Info().
				Int("mr_iid", mr.IID).
				Str("new_title", title).
				Str("prefix", prefix).
				Msg("Removed prefix")

			break
		}
	}

	title = strings.ToUpper(title[0:1]) + title[1:]
	if title != mr.Title {
		r.logWithTag(t).Info().
			Int("mr_iid", mr.IID).
			Str("new_title", title).
			Msg("Capitalized title")

		return &title
	}

	return nil
}

func (r *Release) assertMergeRequestLabels(mr *gitlab.MergeRequest, t tag.Tag) error {
	r.logWithTag(t).Info().Int("mr_iid", mr.IID).Msg("Asserting merge request labels")

	labelPrefixes := []string{
		"type::",
		"docs::",
	}

	for _, labelPrefix := range labelPrefixes {
		label, found := lo.Find(mr.Labels, func(l string) bool {
			return strings.HasPrefix(l, labelPrefix)
		})
		if found {
			r.logWithTag(t).Info().Int("mr_iid", mr.IID).Str("label", label).Msg("Found label")
			return nil
		}
	}

	return fmt.Errorf("label with either prefixes %v not found for merge request !%d (%s)", labelPrefixes, mr.IID, mr.Title)
}

func (r *Release) getTagPipeline(cfg ReleaseConfig) (*gitlab.PipelineInfo, error) {
	r.logWithTag(cfg.Version).Info().Msg("Getting tag pipeline")

	client, err := r.Project.GitLabClient(cfg.Variant)
	if err != nil {
		return nil, err
	}

	gitlabTag, _, err := client.Tags.GetTag(r.Project.ID(cfg.Variant), cfg.Version.String())
	if err != nil {
		return nil, fmt.Errorf("getting tag %s: %w", cfg.Version, err)
	}

	// we need to get the commit as the pipeline is not populated in the tag above
	commit, _, err := client.Commits.GetCommit(r.Project.ID(cfg.Variant), gitlabTag.Commit.ID)
	if err != nil {
		return nil, fmt.Errorf("getting commit %s: %w", gitlabTag.Commit.ID, err)
	}

	return commit.LastPipeline, nil
}

func (r *Release) getBranchPipeline(cfg ReleaseConfig) (*gitlab.PipelineInfo, error) {
	r.logWithTag(cfg.Version).Info().Msg("Getting branch pipeline")

	client, err := r.Project.GitLabClient(cfg.Variant)
	if err != nil {
		return nil, err
	}

	branch, _, err := client.Branches.GetBranch(r.Project.ID(cfg.Variant), cfg.Version.AsStableBranch())
	if err != nil {
		return nil, fmt.Errorf("getting stable branch %s: %w", cfg.Version, err)
	}

	// we need to get the commit as the pipeline is not populated in the tag above
	commit, _, err := client.Commits.GetCommit(r.Project.ID(cfg.Variant), branch.Commit.ID)
	if err != nil {
		return nil, fmt.Errorf("getting commit %s: %w", branch.Commit.ID, err)
	}

	return commit.LastPipeline, nil
}

func (r *Release) WaitTag() error {
	var wg errgroup.Group
	for _, cfg := range r.releasesConfig {
		cfg := cfg
		pipeline, err := r.getTagPipeline(cfg)
		if err != nil {
			return fmt.Errorf("getting tag pipeline: %w", err)
		}

		wg.Go(func() error {
			client, err := r.Project.GitLabClient(cfg.Variant)
			if err != nil {
				return err
			}

			return r.waitPipelineFinish(client, pipeline.ID, gitlab.Success, cfg.Variant)
		})
	}

	return wg.Wait()
}

func (r *Release) WaitStableBranch() error {
	var wg errgroup.Group
	for _, cfg := range r.releasesConfig {
		cfg := cfg
		pipeline, err := r.getBranchPipeline(cfg)
		if err != nil {
			return fmt.Errorf("getting tag pipeline: %w", err)
		}

		wg.Go(func() error {
			client, err := r.Project.GitLabClient(cfg.Variant)
			if err != nil {
				return err
			}

			return r.waitPipelineFinish(client, pipeline.ID, gitlab.Success, cfg.Variant)
		})
	}

	return wg.Wait()
}

func (r *Release) waitPipelineFinish(client *client.GitLab, pipelineID int, desiredSuccessStatus gitlab.BuildStateValue, v Variant) error {
	var logger = func() *zerolog.Logger {
		logger := r.log().With().Int("pipeline_id", pipelineID).Logger()
		return &logger
	}

	var i int
	for {
		if i > 0 {
			timeout := 10 * time.Second
			logger().Info().
				Dur("timeout", timeout).
				Msg("Waiting until next pipeline check...")

			time.Sleep(timeout)
		}
		i++

		finished, status, err := r.checkPipelineStatus(client, pipelineID, desiredSuccessStatus, v)
		if finished {
			logger().Info().Str("status", string(status)).Msg("Pipeline finished")
			return err
		}

		if err != nil {
			logger().Warn().Err(err).Msg("waiting pipeline")
		}
	}
}

func (r *Release) checkPipelineStatus(client *client.GitLab, pipelineID int, desiredSuccessStatus gitlab.BuildStateValue, v Variant) (bool, gitlab.BuildStateValue, error) {
	pipeline, _, err := client.Pipelines.GetPipeline(r.Project.ID(v), pipelineID)
	if err != nil {
		return false, "", err
	}

	status := pipeline.Status
	r.log().Info().Int("pipeline_id", pipelineID).Str("status", status).Msg("Pipeline status")
	switch gitlab.BuildStateValue(status) {
	case desiredSuccessStatus:
		return true, gitlab.BuildStateValue(status), nil
	case gitlab.Failed, gitlab.Canceled, gitlab.Skipped:
		return true, gitlab.BuildStateValue(status), fmt.Errorf("pipeline didn't pass, status: %s", status)
	default:
		return false, "", nil
	}
}

func (r *Release) RunGitlabRelease() error {
	for _, cfg := range r.releasesConfig {
		t := cfg.Version

		r.logWithTag(t).Info().Msg("Running gitlab release")

		pipeline, err := r.getTagPipeline(cfg)
		if err != nil {
			return fmt.Errorf("getting tag pipeline: %w", err)
		}

		client, err := r.Project.GitLabClient(cfg.Variant)
		if err != nil {
			return err
		}

		r.logWithTag(t).Info().Msg("Listing pipeline jobs")
		jobs, _, err := client.Jobs.ListPipelineJobs(r.Project.ID(cfg.Variant), pipeline.ID, &gitlab.ListJobsOptions{
			IncludeRetried: gitlab.Bool(true),
		})
		if err != nil {
			return fmt.Errorf("getting pipeline jobs: %w", err)
		}

		releaseJob, _ := lo.Find(jobs, func(j *gitlab.Job) bool {
			return j.Name == TriggerGitLabReleaseJobName
		})
		if releaseJob == nil {
			jobsNames := lo.Map(jobs, func(j *gitlab.Job, _ int) string {
				return j.Name
			})
			r.logWithTag(t).Warn().Strs("jobs", jobsNames).Msg("Project has no release job...skipping")
			// TODO: mark the CI job as able to fail, this way we'll be able to release even if the release job is not present
			return fmt.Errorf("project has no release job")
		}

		r.logWithTag(t).Info().Str("status", releaseJob.Status).Msg("Release job status")

		switch gitlab.BuildStateValue(releaseJob.Status) {
		case gitlab.Manual:
			r.logWithTag(t).Info().Msg("Playing manual release job")
			_, _, err = client.Jobs.PlayJob(r.Project.ID(cfg.Variant), releaseJob.ID, nil)
			if err != nil {
				return fmt.Errorf("playing release job: %w", err)
			}
		case gitlab.Success, gitlab.Failed, gitlab.Canceled:
			r.logWithTag(t).Info().Msg("Retrying release job")
			_, _, err = client.Jobs.RetryJob(r.Project.ID(cfg.Variant), releaseJob.ID, nil)
			if err != nil {
				return fmt.Errorf("restarting release job: %w", err)
			}
		default:
			r.logWithTag(t).Warn().Msg("Skipping staring release job as it's already in an acceptable status")
		}

		if err := r.waitPipelineFinish(client, pipeline.ID, gitlab.Success, cfg.Variant); err != nil {
			return fmt.Errorf("wait gitlab release pipeline finish: %w", err)
		}
	}

	return nil
}

func (r *Release) MergeStableBranches() error {
	if err := r.Project.Prepare(r.releasesConfig.Versions()...); err != nil {
		return fmt.Errorf("prepare project: %w", err)
	}

	git := r.ProjectGit()

	var commands cmd.ManyWithArgs
	for _, cfg := range r.releasesConfig {
		version := cfg.Version

		// theirs merge strategy ensures that only changelogs will be merged in cases where
		// other files were modified, e.g. Chart.yaml https://gitlab.com/gitlab-org/ci-cd/runner-tools/releases/-/issues/1
		commands = append(commands, r.gitMergeCmd(
			fmt.Sprintf("%s/%s", r.Project.Remote(cfg.Variant), version.AsStableBranch())),
		)
	}

	// merge all changelogs from all versions into main in ascending order
	if err := commands.Run(); err != nil {
		return fmt.Errorf("merge stable branches: %w", err)
	}

	// run the project config only for the Main version to make sure the Versions files are updated properly
	latestReleaseConfig := r.releasesConfig.Latest()

	if err := r.mustCheckoutStableBranch(latestReleaseConfig.Version, latestReleaseConfig.Variant); err != nil {
		return fmt.Errorf("checkout stable branch: %w", err)
	}

	if _, err := r.runProjectConfig(StageMerge, latestReleaseConfig); err != nil {
		return fmt.Errorf("merge stable branches: %w", err)
	}

	if err := r.checkoutMainBranch(); err != nil {
		return fmt.Errorf("checkout main branch: %w", err)
	}

	mergeUpdates := r.gitMergeCmd(latestReleaseConfig.Version.AsStableBranch())
	if err := mergeUpdates.Run(); err != nil {
		return fmt.Errorf("merge stable branches: %w", err)
	}

	if r.dryRun {
		r.log().Warn().Msg("Dry run, skipping push")
		return nil
	}

	if err := git.Args("push", r.Project.Remote(latestReleaseConfig.Variant), r.Project.MainBranch(), latestReleaseConfig.Version.AsStableBranch()).Run(); err != nil {
		return fmt.Errorf("push main branch: %w", err)
	}

	return nil
}

func (r *Release) gitMergeCmd(ref string) cmd.WithArgs {
	return r.ProjectGit().Args(
		"merge", "--commit", "--no-edit", "--no-ff", "--strategy-option", "theirs", ref,
	)
}

func (r *Release) RunReleaseConfigForStage(stage Stage) error {
	if err := r.Project.Prepare(r.releasesConfig.Versions()...); err != nil {
		return fmt.Errorf("prepare project: %w", err)
	}

	for _, cfg := range r.releasesConfig {
		r.logWithTag(cfg.Version).Info().Msg("Running release config for stage " + string(stage))

		if _, err := r.runProjectConfig(stage, cfg); err != nil {
			return fmt.Errorf("run release config for stage %s: %w", stage, err)
		}
	}

	return nil
}
