package release

import (
	"errors"
	"fmt"
	"gitlab-runner-group-releaser/client"
	"gitlab-runner-group-releaser/cmd"
	"gitlab-runner-group-releaser/tag"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/samber/lo"
	"golang.org/x/mod/semver"
)

// TODO: I don't like how the env variables aren't listed in the --help output
const (
	EnvHttpsCloneUsername = "RELEASER_HTTPS_CLONE_USERNAME"
	EnvHttpsClonePassword = "RELEASER_HTTPS_CLONE_PASSWORD"
)

type Project struct {
	Name string `yaml:"name" json:"name"`

	Canonical ProjectVariant `yaml:"canonical" json:"canonical"`
	Security  ProjectVariant `yaml:"security" json:"security"`

	mainBranch  string
	skipPrepare bool

	clientCacheLock sync.Mutex
	clientCache     map[string]*client.GitLab
}

type ProjectVariant struct {
	ID  string `yaml:"id" json:"id"`
	URL string `yaml:"url" json:"url"`
}

type ProjectRemote struct {
	Name string
	URL  string
}

func (p *Project) logger() *zerolog.Logger {
	logger := log.With().Stringer("project", p).Logger()
	return &logger
}

func (p *Project) getProjectVariant(v Variant) ProjectVariant {
	switch v {
	case VariantCanonical:
		return p.Canonical
	case VariantSecurity:
		return p.Security
	default:
		panic("unreachable")
	}
}

func (p *Project) ID(v Variant) string {
	return p.getProjectVariant(v).ID
}

func (p *Project) URL(v Variant) string {
	return p.getProjectVariant(v).URL
}

func (p *Project) GitLabClient(v Variant) (*client.GitLab, error) {
	p.clientCacheLock.Lock()
	defer p.clientCacheLock.Unlock()

	if p.clientCache == nil {
		p.clientCache = make(map[string]*client.GitLab)
	}

	id := p.ID(v)
	if p.clientCache[id] == nil {
		gitlabClient, err := client.NewGitLab(os.Getenv(EnvReleaseGitLabToken), id)
		if err != nil {
			return nil, err
		}

		p.clientCache[id] = gitlabClient
	}

	return p.clientCache[id], nil
}

func (p *Project) String() string {
	return fmt.Sprintf("project: %s", p.Name)
}

func (p *Project) Path() string {
	pwd, _ := os.Getwd()
	return strings.TrimSuffix(filepath.Join(pwd, ".tmp", p.Name), ".git")
}

func (p *Project) Remote(v Variant) string {
	switch v {
	case VariantCanonical:
		return "origin"
	case VariantSecurity:
		return "security"
	default:
		return "origin"
	}
}

func (p *Project) MainBranch() string {
	return p.mainBranch
}

func (p *Project) MainBranchRemote(v Variant) string {
	return fmt.Sprintf("%s/%s", p.Remote(v), p.MainBranch())
}

func (p *Project) authorizedURL(u string) (string, error) {
	if strings.HasPrefix(u, "https://") {
		u, err := url.Parse(u)
		if err != nil {
			return "", err
		}

		u.Host = fmt.Sprintf("%s:%s@%s", os.Getenv(EnvHttpsCloneUsername), os.Getenv(EnvHttpsClonePassword), u.Host)
		url, _ := url.PathUnescape(u.String())
		return url, nil
	}

	return u, nil
}

func (p *Project) CloneURL(v Variant) (string, error) {
	return p.authorizedURL(p.URL(v))
}

func (p *Project) Prepare(versions ...tag.Tag) error {
	if p.skipPrepare {
		p.logger().Warn().Msg("Skipping project prepare")
		return nil
	}

	p.logger().Info().Msg("Preparing project")

	cloneURL, err := p.CloneURL(VariantCanonical)
	if err != nil {
		return err
	}

	// TODO: remove all kinds of local resources that we should pull from the remote
	// TODO: we could probably pull less stuff
	dir := p.Path()
	if gitStatusErr := cmd.New("git").Wd(dir).Args("status").Run(); gitStatusErr != nil {
		p.logger().Info().Str("path", dir).Msg("Project path isn't a git repository, cloning project...")

		_ = os.RemoveAll(dir)

		parent := strings.TrimSuffix(dir, fmt.Sprintf("/%s", filepath.Base(dir)))

		if err := os.MkdirAll(parent, 0774); err != nil {
			return err
		}

		if err := cmd.New("git").Wd(parent).Args("clone", cloneURL, p.Name).Run(); err != nil {
			return err
		}
	}

	git := cmd.New("git").Wd(dir)

	cleanCommands := cmd.ManyWithArgs{
		git.Args("clean", "-f"),
		git.Args("checkout", "--", "."),
	}
	if err := cleanCommands.Run(); err != nil {
		return err
	}

	var remotes []ProjectRemote
	if p.Canonical.URL != "" {
		remotes = append(remotes, ProjectRemote{
			Name: "origin",
			URL:  p.Canonical.URL,
		})
	}
	if p.Security.URL != "" {
		remotes = append(remotes, ProjectRemote{
			Name: "security",
			URL:  p.Security.URL,
		})
	}

	if len(remotes) == 0 {
		return errors.New("no project URLs configured")
	}

	for _, remote := range remotes {
		// we need to manually set the remote for the cases where a project is already cloned from a specific URL,
		// but we change that URL. In this case the project will not be cloned again but the remote will be set to the
		// original
		if err := git.Args("remote", "rm", remote.Name).Run(); err != nil {
			var exitError *exec.ExitError
			if errors.As(err, &exitError) && exitError.ExitCode() != 2 {
				// exit status 2 is "No such remote"
				return fmt.Errorf("removing git remote %q %w", remote.Name, err)
			}
		}

		authorizedRemoteURL, err := p.authorizedURL(remote.URL)
		if err != nil {
			return err
		}

		if err := git.Args("remote", "add", remote.Name, authorizedRemoteURL).Run(); err != nil {
			return err
		}
	}

	if _, err := git.Args("fetch", "--all", "--tags", "--force").RunOutputCapture(); err != nil {
		return err
	}

	mainBranchOutput, err := git.Args("branch", "-r").RunOutputCapture()
	if err != nil {
		return err
	}

	if strings.Contains(mainBranchOutput, fmt.Sprintf("%s/main", p.Remote(VariantCanonical))) {
		p.mainBranch = "main"
	} else if strings.Contains(mainBranchOutput, fmt.Sprintf("%s/master", p.Remote(VariantCanonical))) {
		p.mainBranch = "master"
	} else {
		return fmt.Errorf("could not find main or master branch in project %s", p.URL(VariantCanonical))
	}

	var commands cmd.ManyWithArgs
	commands = append(commands, git.Args("checkout", p.MainBranch()))

	// remove all local branches based on the ones we are going to release
	for _, v := range versions {
		commands = append(commands, git.ArgsInfallible("branch", "-D", v.AsStableBranch()))
	}

	commands = append(commands, cmd.ManyWithArgs{
		git.Args("reset", "--hard", p.MainBranchRemote(VariantCanonical)),
		git.Args("checkout", p.MainBranch()),
	}...)

	return commands.Run()
}

func (p *Project) tags() ([]tag.Tag, error) {
	git := cmd.New("git").Wd(p.Path())

	// tags are already fetched locally in the Prepare stage
	tagsRaw, err := git.Args("tag").RunOutputCapture()
	if err != nil {
		return nil, err
	}

	// there are invalid tags, skip them
	validTags := lo.FilterMap(strings.Split(tagsRaw, "\n"), func(tag string, _ int) (string, bool) {
		return strings.TrimSpace(tag), semver.IsValid(tag)
	})

	semver.Sort(validTags)

	return lo.Map(validTags, func(t string, _ int) tag.Tag {
		tag, _ := tag.Parse(t)
		return tag
	}), nil
}

func (p *Project) PreviousTag(startingTag tag.Tag) (tag.Tag, error) {
	tags, err := p.tags()
	if err != nil {
		return tag.Tag{}, err
	}

	if startingTag.IsMajor() {
		// For major patches find the last version that is not a patch
		// Since the versions are sorted iterating in reverse should land us the correct one
		for _, t := range lo.Reverse(tags) {
			if !t.IsPatch() {
				return t, nil
			}
		}
	}

	// TODO: This can be smarter but I can't be smarter at 2:15 am
	// iterate 500 times, so we don't end up in a loop
	t := startingTag
	for i := 0; i < 500; i++ {
		pt := t.Decrement()
		if lo.ContainsBy(tags, func(t tag.Tag) bool {
			return pt.String() == t.String()
		}) {
			return pt, nil
		}
	}

	return tag.Tag{}, fmt.Errorf("couldn't find a previous version for %s, that's unexpected", startingTag)
}
