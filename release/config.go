package release

import (
	"bytes"
	"fmt"
	"github.com/goccy/go-yaml"
	"github.com/rs/zerolog/log"
	"github.com/samber/lo"
	"gitlab-runner-group-releaser/tag"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/generator"
	"golang.org/x/mod/semver"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

type Context struct {
	Stage   ContextStage   `yaml:"stage" json:"stage"`
	Release ContextRelease `yaml:"runner" json:"runner"`
}

type ContextStage struct {
	Name Stage `yaml:"name" json:"name"`
}

type ContextRelease struct {
	VersionObject tag.Tag `yaml:"version_object" json:"version_object"`
	Version       string  `yaml:"version" json:"version"`

	AppVersionObject tag.Tag `yaml:"app_version_object" json:"app_version_object"`
	AppVersion       string  `yaml:"app_version" json:"app_version"`
}

type ProjectConfig struct {
	Actions []Action

	AdditionalChangelogEntries []generator.OptsAdditionalEntry
}

func (c *ProjectConfig) Execute(release *Release) error {
	for _, action := range c.Actions {
		if err := action.Execute(c, release); err != nil {
			return err
		}
	}

	return nil
}

type configYaml struct {
	Actions []map[string]map[string]any `yaml:"actions" json:"actions"`
}

type ProjectConfigPath string

type Stage string

const (
	StageChangelog    Stage = "changelog"
	StageStableBranch Stage = "branch"
	StageTag          Stage = "tag"
	StageMerge        Stage = "merge"
)

func ProjectConfigPathFromStage(projectDir string, stage Stage) ProjectConfigPath {
	file := fmt.Sprintf("%s.release.yml", stage)
	return ProjectConfigPath(filepath.Join(projectDir, ".gitlab", file))
}

// ParseProjectConfig parses project release config from a stage file.
// It firstly parses the yaml file alongside any Includes specified at the top.
// Then the merged yaml data is parsed through the Go template engine with the provided context.
func ParseProjectConfig(ctx Context, configPath ProjectConfigPath) (*ProjectConfig, error) {
	// The yaml library we are using supports anchors and references,
	// however we opted into using includes manually and the Go template engine instead because:
	// 1. Include is a familiar construct for GitLab engineers even though it's not a part of the YAML spec.
	// 2. It makes it clearly visible which files are being referenced, otherwise that's fairly transparent as the files
	// or parent directory are just passed to the yaml decoder.
	// 3. This approach allows us to evaluate the whole file as a Go template which is slightly easier in terms of implementation.

	b, err := os.ReadFile(string(configPath))
	if err != nil {
		if os.IsNotExist(err) {
			log.Info().Str("file", string(configPath)).Msg("project config file not found, skipping")
			return &ProjectConfig{}, nil
		}

		return nil, err
	}

	// Parse only the Includes section of the file.
	var includesParsed struct {
		Include []string `yaml:"include,omitempty" json:"include,omitempty"`
	}

	// The yaml decoder is not strict by default and will not return an error if an anchor is not defined.
	if err := yaml.Unmarshal(b, &includesParsed); err != nil {
		return nil, err
	}

	var yamlBytesWithIncludes []byte
	if len(includesParsed.Include) > 0 {
		// Prepend the contents of the included files to the contents of the file being parsed.
		for _, include := range includesParsed.Include {
			includeBytes, err := os.ReadFile(filepath.Join(filepath.Dir(string(configPath)), include))
			if err != nil {
				return nil, err
			}

			yamlBytesWithIncludes = append(yamlBytesWithIncludes, includeBytes...)
			yamlBytesWithIncludes = append(yamlBytesWithIncludes, []byte("\n\n")...)
		}
	}

	yamlBytesWithIncludes = append(yamlBytesWithIncludes, b...)

	// Parse the whole file as a Go template.
	tmpl, err := template.New("release").Parse(string(yamlBytesWithIncludes))
	if err != nil {
		return nil, err
	}

	var evaluatedConfigYaml bytes.Buffer
	if err := tmpl.Execute(&evaluatedConfigYaml, ctx); err != nil {
		return nil, err
	}

	// Parse the whole rendered YAML file as a final struct.
	var c configYaml
	if err := yaml.Unmarshal(evaluatedConfigYaml.Bytes(), &c); err != nil {
		return nil, err
	}

	// Each action is parsed separately and then added to the final config struct.
	// This format of naming each action is a bit uglier to parse but allows for more user-friendly yaml files.
	var actions []Action
	for _, actionEntry := range c.Actions {
		var action Action
		var entry any

		if changelog, ok := actionEntry["changelog_entry"]; ok {
			action = &ChangelogEntryAction{}
			entry = changelog
		} else if write, ok := actionEntry["write"]; ok {
			action = &WriteFileAction{}
			entry = write
		} else if commit, ok := actionEntry["commit"]; ok {
			action = &CommitAction{}
			entry = commit
		} else {
			return nil, fmt.Errorf("unknown action: %v", action)
		}

		b, _ := yaml.Marshal(entry)

		if err := yaml.Unmarshal(b, action); err != nil {
			return nil, err
		}

		actions = append(actions, action)
	}

	return &ProjectConfig{
		Actions: actions,
	}, nil
}

type Config struct {
	Releases ReleasesConfig `yaml:"releases" json:"releases"`
	Projects []Project      `yaml:"projects" json:"projects"`
}

type Variant string

var (
	VariantCanonical Variant = "canonical"
	VariantSecurity  Variant = "security"
)

type ReleaseConfig struct {
	Name       string  `yaml:"name" json:"name"`
	Version    tag.Tag `yaml:"version" json:"version"`
	AppVersion tag.Tag `yaml:"app_version" json:"app_version"`
	Variant    Variant `yaml:"variant" json:"variant"`
}

func (r ReleaseConfig) Milestone() tag.Tag {
	// A release can contain a Version or an AppVersion
	// the version refers to the version of the specific project
	// while AppVersion refers to the version of the Runner for that
	// specific project. For example in Chart or Operator.

	if r.AppVersion.IsEmpty() {
		return r.Version
	}

	return r.AppVersion
}

// ReleasesConfig is a slice of ReleaseConfig that is being sorted in ascending order
type ReleasesConfig []ReleaseConfig

func (r ReleasesConfig) Len() int {
	return len(r)
}

func (r ReleasesConfig) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

func (r ReleasesConfig) Less(i, j int) bool {
	return semver.Compare(r[i].Version.String(), r[j].Version.String()) == -1
}

func (r ReleasesConfig) Versions() []tag.Tag {
	return lo.Map(r, func(c ReleaseConfig, _ int) tag.Tag {
		return c.Version
	})
}

func (r ReleasesConfig) Latest() ReleaseConfig {
	return r[len(r)-1]
}

const defaultConfigPath = "config.yml"

func LoadConfig(configPath string) (*Config, error) {
	if configPath == "" {
		log.Warn().Msg("release config path not specified, using default")
		configPath = defaultConfigPath
	}

	log.Info().Str("config_path", configPath).Msg("loading release config from path")

	var b []byte
	if strings.HasPrefix(configPath, "http://") || strings.HasPrefix(configPath, "https://") {
		res, err := http.Get(configPath)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()

		b, err = io.ReadAll(res.Body)
		if err != nil {
			return nil, err
		}
	} else {
		var err error
		b, err = os.ReadFile(configPath)
		if err != nil {
			return nil, err
		}
	}

	var c Config
	if err := yaml.Unmarshal(b, &c); err != nil {
		return nil, err
	}

	return &c, nil
}
