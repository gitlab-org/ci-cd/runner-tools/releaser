#!/bin/bash

set -eox pipefail

# This script is used to initialize a git repository for testing purposes.

remote=${ORIGIN:-git@gitlab.com:ggeorgiev_gitlab/runner-release-test-project.git}
dir=".tmp/runner-release-test-project"

# initialize the repository
rm -rf "$dir"
mkdir -p "$dir"
cd "$dir"
git init
git remote add origin "$remote"
git fetch --all

# create a pristine "main" branch
echo "# README" > README.md
mkdir -p .gitlab
cp -r ../../scripts/runner-release-test-project-template/ .
git add .
git commit -m "Initial commit"

# remove all remote tags
tags=$(git tag -l)
if [[ -n "$tags" ]]; then
  git push origin --delete $tags
  git tag -d $tags
fi

# remove all remote branches
branches=$(git branch -r | grep -v HEAD | grep -v main | sed 's/origin\///g' || true)
if [[ -n "$branches" ]]; then
  git push origin --delete $branches
fi

# this is the default tag - the starting point of the next release
git tag v1.0.0

# create a merge commit to make it like https://gitlab.com/ggeorgiev_gitlab/runner-release-test-project/-/merge_requests/1 has been merged
git checkout -b mr
echo "MR" > MR.md
git add MR.md
git commit -m "MR"
git checkout main
git merge --no-ff mr -m "See merge request https://gitlab.com/ggeorgiev_gitlab/runner-release-test-project/-/merge_requests/1"

# push everything to the remote
git push origin main mr v1.0.0 -f
