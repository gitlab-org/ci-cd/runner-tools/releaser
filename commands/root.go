package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab-runner-group-releaser/release"
	"os"
)

var rootCmd = &cobra.Command{
	Use:  "gitlab-runner-group-releaser",
	Args: cobra.MinimumNArgs(1),
}

type runWithReleaseCallback func(cmd *cobra.Command, args []string, release *release.Release) error

func assertArgsLen(args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("missing project name")
	}

	return nil
}

func RunWithRelease(callback runWithReleaseCallback) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		if err := assertArgsLen(args); err != nil {
			return err
		}

		projectName := args[0]
		versions := args[1:]

		release, err := release.New(projectName, versions...)
		if err != nil {
			return err
		}

		if rootCmd.PersistentFlags().Lookup("dry-run").Value.String() == "true" {
			release.DryRun()
		}

		if rootCmd.PersistentFlags().Lookup("skip-prepare").Value.String() == "true" {
			release.SkipPrepare()
		}

		return callback(cmd, args, release)
	}
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.AddCommand(changelogCmd)
	rootCmd.AddCommand(stableBranchCmd)
	rootCmd.AddCommand(tagCmd)
	rootCmd.AddCommand(waitTagCmd)
	rootCmd.AddCommand(waitStableBranchCmd)
	rootCmd.AddCommand(releaseGitlabCmd)
	rootCmd.AddCommand(mergeStableBranchBackToMainCmd)
	rootCmd.AddCommand(runReleaseConfigCmd)

	rootCmd.PersistentFlags().Bool("dry-run", false, "Do not actually create any tags or branches, just print what would be done")
	rootCmd.PersistentFlags().Bool("skip-prepare", false, "Skip preparing projects at the start of the release. Will not cleanup, clone, pull or in any way change the local copy of a project. Useful for limited testing. This can have unexpected results if you are not sure what you are doing!")
}
