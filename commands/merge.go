package commands

import (
	"github.com/spf13/cobra"
	"gitlab-runner-group-releaser/release"
)

var mergeStableBranchBackToMainCmd = &cobra.Command{
	Use:   "merge-stable-in-main [project name] [versions]...",
	Short: "Merges the stable branches back into the main branch",
	RunE: RunWithRelease(func(cmd *cobra.Command, args []string, release *release.Release) error {
		return release.MergeStableBranches()
	}),
}
