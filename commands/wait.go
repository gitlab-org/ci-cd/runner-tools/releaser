package commands

import (
	"github.com/spf13/cobra"
	"gitlab-runner-group-releaser/release"
)

var waitTagCmd = &cobra.Command{
	Use:   "wait-tag [project name] [versions]...",
	Short: "Waits for the specified tags' pipelines to be finished",
	RunE: RunWithRelease(func(cmd *cobra.Command, args []string, release *release.Release) error {
		return release.WaitTag()
	}),
}

var waitStableBranchCmd = &cobra.Command{
	Use:   "wait-stable-branch [project name] [versions]...",
	Short: "Waits for the specified stable branches' pipelines to be finished",
	RunE: RunWithRelease(func(cmd *cobra.Command, args []string, release *release.Release) error {
		return release.WaitStableBranch()
	}),
}
