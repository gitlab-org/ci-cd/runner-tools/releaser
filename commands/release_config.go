package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab-runner-group-releaser/release"
)

var runReleaseConfigCmd = &cobra.Command{
	Use:   "run-release-config [project name] [stage] [versions]...",
	Short: "Runs ",
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := assertArgsLen(args); err != nil {
			return err
		}

		if len(args) < 3 {
			return fmt.Errorf("missing stage")
		}

		stage := args[1]
		return RunWithRelease(func(cmd *cobra.Command, args []string, r *release.Release) error {
			return r.RunReleaseConfigForStage(release.Stage(stage))
		})(cmd, append([]string{args[0]}, args[2:]...))
	},
}
