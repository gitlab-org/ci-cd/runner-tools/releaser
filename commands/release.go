package commands

import (
	"github.com/spf13/cobra"
	"gitlab-runner-group-releaser/release"
)

var releaseGitlabCmd = &cobra.Command{
	Use:   "release-gitlab [project name] [versions]...",
	Short: "Runs the 'stable gitlab release' job. Currently unused. Will be reworked to allow any job name and 'stable gitlab release' will be made a manual job.",
	RunE: RunWithRelease(func(cmd *cobra.Command, args []string, release *release.Release) error {
		return release.RunGitlabRelease()
	}),
}
