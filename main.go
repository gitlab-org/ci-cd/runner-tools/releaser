package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab-runner-group-releaser/commands"
	"os"
)

// Reference for a release checklist: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29504

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	commands.Execute()

	// flag -notify
	// notifyReleaseChannel(tags, channel)
}

func notifyReleaseChannel(tags string, channel string) {
	// Notify the #f_runner_release channel that the release is done, compile and provide relevant links, such as changelogs and release pages
}
