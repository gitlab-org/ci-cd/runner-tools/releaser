package client

import "github.com/xanzy/go-gitlab"

type GitLab struct {
	projectID string
	*gitlab.Client
}

func NewGitLab(token, projectID string) (*GitLab, error) {
	glab, err := gitlab.NewClient(token)
	if err != nil {
		return nil, err
	}

	return &GitLab{
		Client:    glab,
		projectID: projectID,
	}, nil
}
