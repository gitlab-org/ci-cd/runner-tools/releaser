package tag

import (
	"fmt"
	"strconv"
	"strings"

	"golang.org/x/mod/semver"
)

type Tag struct {
	major int
	minor int
	patch int
}

func Parse(tagRaw string) (Tag, error) {
	tag := strings.TrimSpace(tagRaw)
	if !semver.IsValid(tag) {
		return Tag{}, fmt.Errorf("tag is not a valid semver version: %s", tagRaw)
	}

	// remove leading v, we know it's there otherwise it wouldn't be a valid semver version
	segmentsSplit := strings.Split(tag[1:], ".")
	if len(segmentsSplit) < 3 {
		return Tag{}, fmt.Errorf("tag doesn't have enough segments, major, minor and patch segments are required: %s", tagRaw)
	}

	major, err := strconv.Atoi(segmentsSplit[0])
	if err != nil {
		return Tag{}, fmt.Errorf("invalid tag major version %s: %s", tagRaw, segmentsSplit[0])
	}

	minor, err := strconv.Atoi(segmentsSplit[1])
	if err != nil {
		return Tag{}, fmt.Errorf("invalid tag minor version %s: %s", tagRaw, segmentsSplit[1])
	}

	patch, err := strconv.Atoi(segmentsSplit[2])
	if err != nil {
		return Tag{}, fmt.Errorf("invalid tag patch version %s: %s", tagRaw, segmentsSplit[2])
	}

	return Tag{
		major: major,
		minor: minor,
		patch: patch,
	}, nil
}

func (t *Tag) UnmarshalYAML(b []byte) error {
	tt, err := Parse(string(b))
	if err != nil {
		return err
	}

	*t = tt
	return nil
}

func (t Tag) String() string {
	return fmt.Sprintf("v%d.%d.%d", t.major, t.minor, t.patch)
}

func (t Tag) StringNoPrefix() string {
	return fmt.Sprintf("%d.%d.%d", t.major, t.minor, t.patch)
}

func (t Tag) AsStableBranch() string {
	return fmt.Sprintf("%d-%d-stable", t.major, t.minor)
}

func (t Tag) AsMinor() string {
	return fmt.Sprintf("%d.%d", t.major, t.minor)
}

func (t Tag) IsPatch() bool {
	return t.patch > 0
}

func (t Tag) IsMajor() bool {
	return t.major != 0 && t.minor == 0 && t.patch == 0
}

func (t Tag) NextMinor() Tag {
	return Tag{
		major: t.major,
		minor: t.minor + 1,
		patch: 0,
	}
}

func (t Tag) Decrement() Tag {
	segments := []int{
		t.patch,
		t.minor,
		t.major,
	}

	for i, s := range segments {
		if s > 0 {
			segments[i] -= 1
			if segments[i] < 0 {
				segments[i] = 0
				if i < len(segments) {
					segments[i+1] -= 1
				}
			}

			break
		}
	}

	return Tag{
		major: segments[2],
		minor: segments[1],
		patch: segments[0],
	}
}

func (t Tag) IsEmpty() bool {
	return t.major == 0 && t.minor == 0 && t.patch == 0
}
