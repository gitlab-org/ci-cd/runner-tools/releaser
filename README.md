# GitLab Runner Releaser

The releaser project is consumed by `GitLab Runner Releases` and is expected to be used as either a binary or Docker image containing that binary.

```
Usage:
  gitlab-runner-group-releaser [command]

Available Commands:
  changelog          Generates only the changelog for specified releases and writes them to stdout
  completion         Generate the autocompletion script for the specified shell
  help               Help about any command
  release-gitlab     Runs the 'stable gitlab release' job. Currently unused. Will be reworked to allow any job name and 'stable gitlab release' will be made a manual job.
  stable-branch      Creates stable branches from the main branch or from an already existing stable branch, depending on whether it's a patch release. Changelog is generated for the new release and committed to the branch. Running this command multiple times will update the changelog correctly.
  tag                Creates tags for specified releases
  wait-stable-branch Waits for the specified stable branches' pipelines to be finished
  wait-tag           Waits for the specified tags' pipelines to be finished

Flags:
      --dry-run   Do not actually create any tags or branches, just print what would be done
  -h, --help      help for gitlab-runner-group-releaser
```

Each command of the Releaser must be runnable multiple times and always produce the expected results.

For stable branches, tags and changelog especially running these commands multiple times must:

1. Not fail
2. Create the needed branches tags and files if they don't exist
3. Update them accordingly if they do exist

For example, the changelog file is updated by inserting the correct changes in the current release or by creating a new release.

## General usage

Each of the subcommands accepts multiple arguments:

```
Creates tags for specified releases

Usage:
  gitlab-runner-group-releaser tag [project name] [versions]... [flags]

Flags:
  -h, --help   help for tag

Global Flags:
      --dry-run   Do not actually create any tags or branches, just print what would be done
```

The project name is the name of the resource associated with a release. For example, in the following `config.yml`:

```yml
projects:
  - name: runner
    url: https://gitlab.com/gitlab-org/gitlab-runner.git
    id: 250833
  - name: chart
    url: https://gitlab.com/gitlab-org/charts/gitlab-runner.git
    id: 6329679

releases:
  - name: runner
    version: v15.10.0
  - name: chart
    version: v0.51.0
    app_version: v15.10.0

  - name: runner
    version: v15.10.1
  - name: chart
    version: v0.51.1
    app_version: v15.10.1
```

Running `releaser tag runner` will create tags for both `v15.10.0` and `v15.10.1` releases of the `runner` project.

Running `releaser tag chart` will create tags for both `v0.51.0` and `v0.51.1` releases of the `chart` project.

If we want to be more specific, we can filter out specific versions by passing them as additional arguments: `releaser tag runner v15.10.1`. This is most useful for debugging.

## Additional configuration

Apart from the CLI arguments the following env variables are accepted:

- `RELEASER_GITLAB_TOKEN` - GitLab API token used to access the GitLab API. This is required for all commands.
- `RELEASER_CONFIG_PROJECT_PATH` - Path to the project containing the `config.yml` file. This is required for all commands. By default it's `$CWD/config.yml`.
- `RELEASER_HTTPS_CLONE_USERNAME` / `RELEASER_HTTPS_CLONE_PASSWORD` - Username and password used to clone the projects. This is only required if using HTTPS is desired over SSH.

## Project specific configuration

Projects can have specific steps that are executed for each step. For example in the case of Chart we need to update a `Chart.yaml` file and commit it. 

Each project can include additional steps in its `.gitlab` directory with the following files inside:

- `changelog.release.yml`
- `branch.release.yml`
- `tag.release.yml`
- `merge.release.yml`

Each of these actions will be executed on each [retrospective step](https://gitlab.com/gitlab-org/ci-cd/runner-tools/releaser/-/blob/99566af607280df1a6ee1840df68031427099485/release/config.go#L62).

Example: [branch.release.yml](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/main/.gitlab/branch.release.yml). The files can include other yaml files such as [.common.release.yml](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/main/.gitlab/.common.release.yml)

Supported actions:

* `changelog_entry` - Creates a changelog entry with a `scope` and `entry`. `scope` is a changelog scope found in the [changelog.yml](https://gitlab.com/gitlab-org/charts/gitlab-runner/-/blob/main/.gitlab/changelog.yml). `entry` is the text of the changelog entry. 
* `write` - Will write a file `file` with the contents of `content`. `file` is the path to the file to be written relative to the root of the project. `content` is the content of the file.
* `commit` - Will commit `files` with a `message`.

### Interpolation

To allow dynamic changelog entries and commit messages all the project conficts will be interpolated with the following context:

```
    Stage:
        Name
    Release:
        Version
        AppVersion
```

This will take the currently processed release and the stage that is being executed and interpolate the values into the configuration.
