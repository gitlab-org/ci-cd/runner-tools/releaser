FROM golang:1.21 as builder

WORKDIR /app
ADD . .

RUN go test ./... -v

RUN go build -o releaser

FROM ubuntu:23.04

RUN apt-get update -y && \
    apt-get install wget zip git -y

COPY --from=builder /app/releaser /usr/local/bin/releaser

RUN releaser --help
