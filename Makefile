ifndef CI_IMAGE
override CI_IMAGE = gitlab-runner-group-releaser
endif

build_docker:
	docker build -t ${CI_IMAGE} .

release_create_tags:
	releaser -create-tags $(GITLAB_RUNNER_RELEASE_TAGS)

release_wait_tags_pipelines:
	releaser -wait-tags $(GITLAB_RUNNER_RELEASE_TAGS)

release_gitlab:
	releaser -gitlab-release $(GITLAB_RUNNER_RELEASE_TAGS)

release_notify:
	releaser -notify $(GITLAB_RUNNER_RELEASE_TAGS) $(GITLAB_RUNNER_RELEASE_CHANNEL)

init-test-repo:
	@bash scripts/initialize-test-git-repo.sh